package com.sukowidodo.apps.scanme.ext

import com.sukowidodo.apps.scanme.data.model.HistoryData
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Before
import org.junit.Test

/**
 * Created by Suko Widodo on 02/04/23.
 */
class ExpressionParseTest {

    private lateinit var expressionParse: ExpressionParse

    @Before
    fun setUp() {
        expressionParse = ExpressionParse()
    }

    @Test
    fun `parsing should return correct result`() {
        val input = "2+3"
        val expected = HistoryData(first = 2, second = 3, operator = '+', result = 5)
        assertEquals(expected, expressionParse.parsing(input))
    }

    @Test
    fun `parsing should handle negative numbers`() {
        val input = "-2+5"
        val expected = HistoryData(first = -2, second = 5, operator = '+', result = 3)
        assertEquals(expected, expressionParse.parsing(input))
    }

    @Test
    fun `parsing should handle minus`() {
        val input = "7-5"
        val expected = HistoryData(first = 7, second = 5, operator = '-', result = 2)
        assertEquals(expected, expressionParse.parsing(input))
    }

    @Test
    fun `parsing should handle multiplication`() {
        val input = "2*3"
        val expected = HistoryData(first = 2, second = 3, operator = '*', result = 6)
        assertEquals(expected, expressionParse.parsing(input))
    }

    @Test
    fun `parsing should handle division`() {
        val input = "10/2"
        val expected = HistoryData(first = 10, second = 2, operator = '/', result = 5)
        assertEquals(expected, expressionParse.parsing(input))
    }

    @Test
    fun `parsing should handle if there are multiple expression take one`() {
        val input = "10-1*5"
        val expected = HistoryData(first = 10, second = 1, operator = '-', result = 9)
        assertEquals(expected, expressionParse.parsing(input))
    }

}
