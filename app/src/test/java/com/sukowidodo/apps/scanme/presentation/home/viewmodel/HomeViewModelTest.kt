package com.sukowidodo.apps.scanme.presentation.home.viewmodel
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.sukowidodo.apps.scanme.data.model.HistoryData
import com.sukowidodo.apps.scanme.data.repository.HistoryRepository
import com.sukowidodo.apps.scanme.ext.ExpressionParse
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineScope
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class HomeViewModelTest {

    // Subject under test
    private lateinit var viewModel: HomeViewModel

    // Collaborators
    @Mock private lateinit var repository: HistoryRepository
    private lateinit var exParse: ExpressionParse

    @get:Rule var rule: TestRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        exParse = ExpressionParse()
        viewModel = HomeViewModel(repository, exParse)
    }

    @Test
    fun `fetchAllDataFromDb should set allData LiveData`() = runBlocking {
        // Given
        val testData = listOf(HistoryData(0, 1, 2, '+', 3), HistoryData(0, 4, 5, '-', -1))
        `when`(repository.getAllDataDB()).thenReturn(testData)

        // When
        viewModel.fetchAllDataFromDb()

        // Then
        assertEquals(testData, viewModel.allData.value)
    }

    @Test
    fun `saveDataDB should call repository saveDataDB and fetchAllDataFromDb`(){
        runBlocking {
            // Given
            val testData = HistoryData(0,1, 2, '+', 3)

            // When
            viewModel.saveDataDB(testData)

            // Then
            verify(repository).saveDataDB(testData)
            verify(repository).getAllDataDB()
        }
    }

    @Test
    fun `saveDataToFile should call repository saveDataFile and getDataFromFile`() {
        // Given
        val testData = HistoryData(0,1, 2, '+', 3)

        // When
        viewModel.saveDataToFile(testData)

        // Then
        verify(repository).saveDataFile(testData)
        verify(repository).getAllDataFile()
    }

    @Test
    fun `getDataFromFile should set allData LiveData`() = runBlocking {
        // Given
        val testData = listOf(HistoryData(0, 1, 2, '+', 3), HistoryData(2,4, 5, '-', -1))
        `when`(repository.getAllDataFile()).thenReturn(testData)

        // When
        viewModel.getDataFromFile()

        // Then
        assertEquals(testData, viewModel.allData.value)
    }
}
