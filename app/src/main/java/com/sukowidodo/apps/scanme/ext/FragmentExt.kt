package com.sukowidodo.apps.scanme.ext

import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver

/**
 * Created by Suko Widodo on 31/03/23.
 */
fun Lifecycle.addObservers(vararg lifecycleObserver: LifecycleObserver) {
    lifecycleObserver.forEach {
        addObserver(it)
    }
}

fun Fragment.addObservers(vararg lifecycleObserver: LifecycleObserver) {
    viewLifecycleOwner.lifecycle.addObservers(*lifecycleObserver)
}