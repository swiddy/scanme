package com.sukowidodo.apps.scanme.di

import com.sukowidodo.apps.scanme.data.repository.HistoryDataStore
import com.sukowidodo.apps.scanme.data.repository.HistoryRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

/**
 * Created by Suko Widodo on 30/03/23.
 */

@Module
@InstallIn(SingletonComponent::class)
abstract class SingletonModule {
    @Binds
    abstract fun bindHistoryRepo(dataStore: HistoryDataStore): HistoryRepository
}