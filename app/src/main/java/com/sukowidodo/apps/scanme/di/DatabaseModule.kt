package com.sukowidodo.apps.scanme.di

import android.content.Context
import androidx.room.Room
import com.sukowidodo.apps.scanme.data.database.AppDatabase
import com.sukowidodo.apps.scanme.data.database.dao.HistoryDao
import com.sukowidodo.apps.scanme.data.repository.HistoryDataStore
import com.sukowidodo.apps.scanme.data.repository.HistoryRepository
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Named
import javax.inject.Singleton

/**
 * Created by Suko Widodo on 30/03/23.
 */

@Module
@InstallIn(SingletonComponent::class)
class DatabaseModule {
    companion object {
        const val DATABASE = "scanMe"
    }

    @Singleton
    @Provides
    fun provideAppDatabase(@ApplicationContext context: Context): AppDatabase {
        try {
            return Room.databaseBuilder(context, AppDatabase::class.java, DATABASE)
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build()
        } catch (e: Exception) {
            throw e
        }
    }

    @Singleton
    @Provides
    fun provideUserDao(database: AppDatabase): HistoryDao {
        return database.historyDao()
    }
}