package com.sukowidodo.apps.scanme.presentation.home.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.mlkit.vision.common.InputImage
import com.google.mlkit.vision.text.TextRecognition
import com.google.mlkit.vision.text.latin.TextRecognizerOptions
import com.sukowidodo.apps.scanme.R
import com.sukowidodo.apps.scanme.data.model.HistoryData
import com.sukowidodo.apps.scanme.data.repository.HistoryRepository
import com.sukowidodo.apps.scanme.ext.ExpressionParse
import com.sukowidodo.apps.scanme.ext.immutable
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject


/**
 * Created by Suko Widodo on 30/03/23.
 */
@HiltViewModel
class HomeViewModel @Inject constructor(
    private val repository: HistoryRepository,
    private val exParse: ExpressionParse
): ViewModel() {
    val _result = MutableLiveData<Int>()
    val result = _result.immutable()

    val _source = MutableLiveData<Int>()
    val source = _source.immutable()

    val _allData = MutableLiveData<List<HistoryData>>()
    val allData = _allData.immutable()


    suspend fun saveDataDB(historyData: HistoryData){
        repository.saveDataDB(historyData)
        fetchAllDataFromDb()
    }

    fun saveDataToFile(historyData: HistoryData) {
        repository.saveDataFile(historyData)
        getDataFromFile()
    }

    fun getDataFromFile(){
        val data = repository.getAllDataFile()
        _allData.postValue(data)
    }

    private suspend fun getAllDataDb():List<HistoryData> = repository.getAllDataDB()

    fun parseExpression(string: String):HistoryData? {
        return try {
            exParse.parsing(string)
        } catch (e: Exception){
            e.printStackTrace()
            null
        }
    }

    suspend fun fetchAllDataFromDb() {
        val data = getAllDataDb()
        _allData.value = data
    }

    fun recognizeImage(image: InputImage){
        val recognizer = TextRecognition.getClient(TextRecognizerOptions.DEFAULT_OPTIONS)
        recognizer.process(image)
            .addOnSuccessListener {
                val data = parseExpression(it.text)
                if (data != null) {
                    if (source.value == R.id.rbDataBase) {
                        viewModelScope.launch {
                            saveDataDB(historyData = data)
                        }
                    } else {
                        saveDataToFile(data)
                    }
                }
            }
    }
}