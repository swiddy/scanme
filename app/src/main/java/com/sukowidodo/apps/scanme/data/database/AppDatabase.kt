package com.sukowidodo.apps.scanme.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.sukowidodo.apps.scanme.data.database.dao.HistoryDao
import com.sukowidodo.apps.scanme.data.model.HistoryData

/**
 * Created by Suko Widodo on 30/03/23.
 */
@Database(
    entities = [HistoryData::class],
    version = 2
)
abstract class AppDatabase:RoomDatabase() {
    abstract fun historyDao(): HistoryDao
}