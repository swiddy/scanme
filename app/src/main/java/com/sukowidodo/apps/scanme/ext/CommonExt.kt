package com.sukowidodo.apps.scanme.ext

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

/**
 * Created by Suko Widodo on 30/03/23.
 */

fun<T> MutableLiveData<T>.immutable(): LiveData<T>{
    return this
}

fun<T>List<T>.toMutable(): MutableList<T>{
    val mutableLiveData = mutableListOf<T>()
    mutableLiveData.addAll(this)
    return mutableLiveData
}