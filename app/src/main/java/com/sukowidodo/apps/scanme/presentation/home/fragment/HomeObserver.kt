package com.sukowidodo.apps.scanme.presentation.home.fragment

import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner
import com.sukowidodo.apps.scanme.presentation.home.viewmodel.HomeViewModel

/**
 * Created by Suko Widodo on 31/03/23.
 */
class HomeObserver(
    val viewModel: HomeViewModel,
    val contract: HomeFragmentContract
): DefaultLifecycleObserver {
    override fun onCreate(owner: LifecycleOwner) {
        super.onCreate(owner)
        viewModel.allData.observe(owner){
            contract.allDataSuccess(it)
        }
    }
}