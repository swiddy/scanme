package com.sukowidodo.apps.scanme.data.model

/**
 * Created by Suko Widodo on 30/03/23.
 */
enum class Operators(var sign:Char) {
    MINUS('-'),
    PLUS('+'),
    MULTIPLY('*'),
    DIVISION('/')
}