package com.sukowidodo.apps.scanme.data.repository

import com.sukowidodo.apps.scanme.data.model.HistoryData

/**
 * Created by Suko Widodo on 30/03/23.
 */
interface HistoryRepository {
    suspend fun saveDataDB(historyData: HistoryData)
    suspend fun getAllDataDB():List<HistoryData>

    fun saveDataFile(historyData: HistoryData)
    fun getAllDataFile():List<HistoryData>

    fun isFilePresent(): Boolean
}