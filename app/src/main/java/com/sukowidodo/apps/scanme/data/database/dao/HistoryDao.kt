package com.sukowidodo.apps.scanme.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.sukowidodo.apps.scanme.data.model.HistoryData

/**
 * Created by Suko Widodo on 30/03/23.
 */
@Dao
interface HistoryDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveData(historyData: HistoryData)

    @Query("SELECT * FROM historydata")
    fun getAllData(): List<HistoryData>
}