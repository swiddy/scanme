package com.sukowidodo.apps.scanme.presentation.home.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.sukowidodo.apps.scanme.databinding.ItemHistoryBinding
import com.sukowidodo.apps.scanme.data.model.HistoryData
import javax.inject.Inject

/**
 * Created by Suko Widodo on 30/03/23.
 */
class HistoryAdapter @Inject constructor()
    : RecyclerView.Adapter<HistoryAdapter.MyViewHolder>() {

    var list: List<HistoryData> = listOf()

    inner class MyViewHolder(private val binding:ItemHistoryBinding) :ViewHolder(binding.root){
        fun bind(data: HistoryData){
            binding.tvInput.text = "Input = ${data.first} ${data.operator} ${data.second}"
            binding.tvResult.text = "Result = ${data.result}"
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val binding = ItemHistoryBinding.inflate(LayoutInflater.from(parent.context), parent,false)
        return MyViewHolder(binding)
    }

    override fun getItemCount() = list.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
       holder.bind(list[position])
    }
}