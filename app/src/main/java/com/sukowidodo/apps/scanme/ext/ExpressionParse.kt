package com.sukowidodo.apps.scanme.ext

import com.sukowidodo.apps.scanme.data.model.HistoryData
import com.sukowidodo.apps.scanme.data.model.Operators
import javax.inject.Inject

/**
 * Created by Suko Widodo on 31/03/23.
 */
class ExpressionParse @Inject constructor() {

    private val listOperator = Operators.values().map { it.sign }

    fun parsing(string: String) : HistoryData {
        var first = ""
        var operator: Char? = null
        var second = ""

        var array = string.toCharArray()
        for (i in 0..array.lastIndex) {
            if (i == 0 && array[i] == '-'){
                first += '-'
                continue
            }
            if (!listOperator.contains(array[i])) {
                if (operator == null) first += array[i] else second += array[i]
            } else {
                if (operator == null) operator = array[i]
                else break
            }
        }
        return doCalculate(first,second, operator)
    }

    private fun doCalculate(firstString:String, secondString:String, operator:Char?): HistoryData {
        val first = firstString.filter { it.isDigit() || it == '-' }.toInt()
        val second = secondString.filter { it.isDigit() }.toInt()

        val result = when (operator) {
            Operators.PLUS.sign -> sumData(first, second)
            Operators.MINUS.sign -> minData(first, second)
            Operators.MULTIPLY.sign -> multiplyData(first, second)
            Operators.DIVISION.sign -> divData(first, second)
            else -> -1
        }

        return HistoryData(
            first = first,
            second = second,
            operator = operator,
            result = result
        )
    }

    private fun sumData(first:Int, second:Int) = first + second

    private fun minData(first:Int, second:Int) = first - second

    private fun divData(first:Int, second:Int) = first / second

    private fun multiplyData(first:Int, second:Int) = first * second
}