package com.sukowidodo.apps.scanme.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by Suko Widodo on 30/03/23.
 */
@Entity
data class HistoryData(
    @PrimaryKey(autoGenerate = true)
    val id: Int? = null,
    val first:Int?,
    val second:Int?,
    val operator:Char?,
    val result:Int?
)
