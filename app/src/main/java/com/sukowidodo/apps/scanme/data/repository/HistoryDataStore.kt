package com.sukowidodo.apps.scanme.data.repository

import android.annotation.SuppressLint
import android.content.Context
import android.content.ContextWrapper
import android.graphics.BitmapFactory
import android.os.Environment
import android.util.Log
import android.widget.Toast
import androidx.security.crypto.EncryptedFile
import androidx.security.crypto.MasterKey
import androidx.security.crypto.MasterKeys
import com.google.gson.GsonBuilder
import com.sukowidodo.apps.scanme.BuildConfig
import com.sukowidodo.apps.scanme.data.database.dao.HistoryDao
import com.sukowidodo.apps.scanme.data.model.HistoryData
import com.sukowidodo.apps.scanme.ext.toMutable
import dagger.hilt.android.qualifiers.ApplicationContext
import java.io.*
import java.nio.charset.StandardCharsets
import java.security.InvalidKeyException
import java.security.NoSuchAlgorithmException
import javax.crypto.Cipher
import javax.crypto.CipherInputStream
import javax.crypto.CipherOutputStream
import javax.crypto.NoSuchPaddingException
import javax.crypto.spec.SecretKeySpec
import javax.inject.Inject


/**
 * Created by Suko Widodo on 30/03/23.
 */
class HistoryDataStore @Inject constructor(
    private val historyDao: HistoryDao,
    @ApplicationContext val context: Context
): HistoryRepository {

    companion object {
        const val FILENAME = "from_${BuildConfig.BUILD_VARIANT}.json"
    }

    override suspend fun saveDataDB(historyData: HistoryData) {
        historyDao.saveData(historyData)
    }

    override suspend fun getAllDataDB(): List<HistoryData> {
        return historyDao.getAllData()
    }

    override fun saveDataFile(historyData: HistoryData) {
        try {
            if (!isFilePresent()) {
                create(listDataToJson(listOf(historyData)))
            } else {
                val list = getAllDataFile().toMutable()
                list.add(historyData)
                val data = listDataToJson(list)
                deleteFile()
                create(data)
            }
        } catch (e: Exception){
            e.printStackTrace()
        }
    }

    override fun getAllDataFile(): List<HistoryData> {
        val string = read(context)
        return writeJsonToHistoryData(context, string)
    }

    private fun read(context: Context): String? {
        return try {
            val keyGenParameterSpec = MasterKey.KeyScheme.AES256_GCM
            val mainKeyAlias = MasterKey.Builder(context)
                .setKeyScheme(keyGenParameterSpec)
                .build()

            val root: File = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
            val encryptedFile = EncryptedFile.Builder(
                context,
                File(root, FILENAME),
                mainKeyAlias,
                EncryptedFile.FileEncryptionScheme.AES256_GCM_HKDF_4KB
            ).build()

            val inputStream = encryptedFile.openFileInput()
            val byteArrayOutputStream = ByteArrayOutputStream()
            var nextByte: Int = inputStream.read()
            while (nextByte != -1) {
                byteArrayOutputStream.write(nextByte)
                nextByte = inputStream.read()
            }

            val plaintext: ByteArray = byteArrayOutputStream.toByteArray()
            String(plaintext)
        } catch (fileNotFound: FileNotFoundException) {
            fileNotFound.printStackTrace()
            null
        } catch (ioException: IOException) {
            ioException.printStackTrace()
            null
        } catch (e: Exception){
            e.printStackTrace()
            null
        }
    }

    private fun create(jsonString: String?): Boolean {
        return try {
            val root: File = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
            val keyGenParameterSpec = MasterKey.KeyScheme.AES256_GCM
            val mainKeyAlias = MasterKey.Builder(context)
                .setKeyScheme(keyGenParameterSpec)
                .build()

            val encryptedFile = EncryptedFile.Builder(
                context,
                File(root, FILENAME),
                mainKeyAlias,
                EncryptedFile.FileEncryptionScheme.AES256_GCM_HKDF_4KB
            ).build()

            encryptedFile.openFileOutput().apply {
                write(jsonString?.toByteArray(StandardCharsets.UTF_8))
                flush()
                close()
            }
            true
        } catch (fileNotFound: FileNotFoundException) {
            false
        } catch (ioException: IOException) {
            false
        } catch (ex:Exception){
            ex.printStackTrace()
            false
        }
    }

    override fun isFilePresent(): Boolean {
        val root: File = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
        val file = File(root, FILENAME)
        return file.exists()
    }

    fun deleteFile(): Boolean {
        val root: File = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
        val file = File(root, FILENAME)
        return file.delete()
    }

    private fun writeJsonToHistoryData(context: Context, stringJson: String?): List<HistoryData> {
        return try {
            val gson = GsonBuilder().create()
            val json = read(context)
            gson.fromJson(json, Array<HistoryData>::class.java).asList()
        } catch (e:Exception) {
            e.printStackTrace()
            emptyList()
        }
    }

    private fun listDataToJson(list: List<HistoryData>): String {
        val gson = GsonBuilder().create()
        return gson.toJson(list)
    }
}