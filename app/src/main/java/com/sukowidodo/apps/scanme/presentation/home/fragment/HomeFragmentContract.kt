package com.sukowidodo.apps.scanme.presentation.home.fragment

import com.sukowidodo.apps.scanme.data.model.HistoryData

/**
 * Created by Suko Widodo on 31/03/23.
 */
interface HomeFragmentContract {
    fun allDataSuccess(list: List<HistoryData>)
    fun allDataFailed(throwable: Throwable)
}