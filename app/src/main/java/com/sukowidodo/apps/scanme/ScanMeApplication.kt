package com.sukowidodo.apps.scanme

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * Created by Suko Widodo on 30/03/23.
 */
@HiltAndroidApp
class ScanMeApplication: Application() {
    override fun onCreate() {
        super.onCreate()
    }
}