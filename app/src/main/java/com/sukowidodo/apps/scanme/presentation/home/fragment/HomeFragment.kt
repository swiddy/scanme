package com.sukowidodo.apps.scanme.presentation.home.fragment

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.mlkit.vision.common.InputImage
import com.sukowidodo.apps.scanme.BuildConfig
import com.sukowidodo.apps.scanme.R
import com.sukowidodo.apps.scanme.databinding.FragmentHomeBinding
import com.sukowidodo.apps.scanme.data.model.HistoryData
import com.sukowidodo.apps.scanme.ext.addObservers
import com.sukowidodo.apps.scanme.presentation.home.adapter.HistoryAdapter
import com.sukowidodo.apps.scanme.presentation.home.viewmodel.HomeViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
@AndroidEntryPoint
class HomeFragment : Fragment(), HomeFragmentContract {

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!
    val viewModel:HomeViewModel by viewModels()
    @Inject
    lateinit var historyAdapter: HistoryAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        initObserver()
        initAction()
        initData()
        return binding.root
    }

    private fun initAction(){
        binding.button.setOnClickListener {
//            showPopup(it)
            when(BuildConfig.BUILD_VARIANT){
                "filesystem" -> {
                    ImagePicker.with(this@HomeFragment)
                        .galleryOnly()
                        .createIntent {
                            startForProfileImageResult.launch(it)
                        }
                }
                "built_in_camera" -> {
                    ImagePicker.with(this@HomeFragment)
                        .cameraOnly()
                        .createIntent {
                            startForProfileImageResult.launch(it)
                        }
                }
            }
        }

        binding.rgSource.setOnCheckedChangeListener { _, checkedId ->
            viewModel._source.value = checkedId
            if (checkedId == R.id.rbFile){
                viewModel.getDataFromFile()
            } else {
                lifecycleScope.launch {
                    viewModel.fetchAllDataFromDb()
                }
            }
        }
    }

    private fun showPopup(v: View) {
        val popup = PopupMenu(requireContext(), v)
        val inflater: MenuInflater = popup.menuInflater
        inflater.inflate(R.menu.menu_main, popup.menu)
        popup.setOnMenuItemClickListener { menu ->
            when(menu.itemId) {
                R.id.action_camera -> {
                    ImagePicker.with(this)
                        .cameraOnly()
                        .createIntent {
                            startForProfileImageResult.launch(it)
                        }
                }
                R.id.action_file -> {
                    ImagePicker.with(this)
                        .galleryOnly()
                        .createIntent {
                            startForProfileImageResult.launch(it)
                        }
                }
            }
            false
        }
        popup.show()
    }

    private val startForProfileImageResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            val resultCode = result.resultCode
            val data = result.data

            when (resultCode) {
                Activity.RESULT_OK -> {
                    //Image Uri will not be null for RESULT_OK
                    val fileUri = data?.data!!
                    val image = InputImage.fromFilePath(requireContext(), fileUri)
                    viewModel.recognizeImage(image)
                }
                ImagePicker.RESULT_ERROR -> {
                    Toast.makeText(requireContext(), ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
                }
                else -> {
                    Toast.makeText(requireContext(), "Task Cancelled", Toast.LENGTH_SHORT).show()
                }
            }
        }

    private fun initData() {
        binding.rbDataBase.isChecked = true
        lifecycleScope.launch {
            viewModel.fetchAllDataFromDb()
        }
    }

    private fun initObserver(){
        addObservers(
            HomeObserver(viewModel, this)
        )
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun allDataSuccess(list: List<HistoryData>) {
        historyAdapter.list = list
        binding.rvHistory.apply {
            adapter = historyAdapter
            layoutManager = LinearLayoutManager(requireContext())
        }
        lifecycleScope.launch {
            delay(1000)
            binding.rvHistory.scrollToPosition(historyAdapter.list.size-1)
        }
    }

    override fun allDataFailed(throwable: Throwable) {
        Toast.makeText(requireContext(), "${throwable.message}", Toast.LENGTH_SHORT).show()
    }
}